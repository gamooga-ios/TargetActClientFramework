//
//  TargetActClent.h
//  Notificatons
//
//  Created by Nishanth on 29/12/15.
//  Copyright © 2015 Gamooga. All rights reserved.
//

#ifndef _TargetActClient_h
#define _TargetActClient_h

#import <UIKit/UIKit.h>

//! Project version number for TargetActClient.
FOUNDATION_EXPORT double TargetActClientVersionNumber;

//! Project version string for TargetActClient.
FOUNDATION_EXPORT const unsigned char TargetActClientVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TargetActClient/PublicHeader.h>
extern NSString *feServer;
extern NSString *cid;
extern NSString *beServer;
extern NSString *viServer;
extern NSString *evServer;
extern NSString *sid;
extern NSString *vid;


typedef enum {
    StripInApp = 0,
    ImageInApp,
    TextInApp,
    TextImageInApp,
    FullImageInApp
}GAAlertBannerType;


//In app notifications
@protocol TargetActClientDelegate <NSObject>

-(void)selectedBanner:(GAAlertBannerType)bannerType parmaters:(NSMutableDictionary *)actionInformation;

@end

//[self.sendDataProtocolDelegate sendDataToChooseLocation:[place valueForKey:@"formatted_address"] andLatitide:[latitude doubleValue] andLongitude:[longitude doubleValue]];


@interface TargetActClient: NSObject {
}
@property (strong, nonatomic) id <TargetActClientDelegate> target;

+ (TargetActClient *) sharedInstance;
+ (TargetActClient *) sharedInstanceWithServer:(NSString *)server;
+ (TargetActClient *) sharedInstanceWithCompId:(NSString *)compId;
- (void) doPushRegistration;
- (NSString *) getPushRegistrationId;
- (NSString *) getVisitorID;
- (void) updatePushRegistrationId:(NSData *)token;
- (void) pushEvent:(NSString *)ev withProperties:(NSDictionary *)props;
- (void) pushProperty:(NSString *)prop withValue:(id)val;
- (void) pushProperties:(NSDictionary *)props;
- (void) addPropertyToList:(NSString *)prop withValue:(NSString *)val;
- (void) removePropertyFromList:(NSString *)prop withValue:(NSString *)val;
- (void)identify:(NSString *)uniqId;
- (void)dualIdentification:(NSArray *)arr;
- (void)saveSessionTimeout:(NSInteger)timeInMinutes;
- (void)registerCustomActionCallback:(void (^)(NSDictionary *result))block;
- (void)getEvents:(NSString *)event events_count:(NSInteger)events_count timestamp:(NSTimeInterval)timestamp withCompletion:(void (^)(NSData *result, NSInteger status))block;
- (void)getEvents:(NSString *)event events_count:(NSInteger)events_count propertyName:(NSString*)propertyName propertyValue:(NSString*)propertyValue timestamp:(NSTimeInterval)timestamp withCompletion:(void (^)(NSData *result, NSInteger status))block;
- (void) setRequestTimeout:(int)timeout;
- (void) initializeCarouselActions;
- (BOOL)isGamoogaPush:(NSDictionary *)userInfo;
- (void)pushOpenedEvent:(NSDictionary *)userInfo;
- (void)pushOpenedErrorEvent:(NSDictionary *)userInfo;
- (void)pushReceivedEvent:(NSDictionary *)userInfo;
- (void)pushReceivedErrorEvent:(NSDictionary *)userInfo;
- (void)notificationReceived:(NSDictionary *)notifdata;
- (void)mobileNotifCloseEvent;
- (void)registerPushEvent:(id)val withCallback:(void (^)(NSDictionary *result))block;
- (void) imageurlWithButtons:(NSString *)url launched_on:(NSString *)nxt_url firstButtonTitle:(NSString *)firstButtonTitle secondButtonTitle:(NSString *)secondButtonTitle  firstButtonTitlefont:(UIFont *)firstButtonTitleFont  secondButtonTitleFont:(UIFont *)secondButtonTitleFont firstButtonTitleColour:(UIColor *)firstButtonTitleColour  secondButtonTitleColour:(UIColor *)secondButtonTitleColour firstButtonBackGroundColour:(UIColor *)firstButtonBackGroundColour  secondButtonVBackGroundColour:(UIColor *)secondButtonVBackGroundColour bannerTextImageUrl:(NSString *)bannerTextImageUrl noOfButtons:(NSString *)nctas;

- (void)titlecolor:(UIColor *)titletextcolor viewbackclr:(UIColor *)framebckgclr messageclr:(UIColor *)msgclr borderclr:(UIColor *)borderclr msgtitle:(NSString *)mt msgtxt:(NSString *)mtxt btnbgclr:(UIColor *)btnbgclr btntxtclr:(UIColor *)btntxtclr buttontxt:(NSString *)btntxt launched_on:(NSString *)nxt_url imageUrl:(NSString *)imageUrl;

@end

#endif
