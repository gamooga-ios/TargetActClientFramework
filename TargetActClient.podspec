
Pod::Spec.new do |s|

  s.name         = "TargetActClient"
  s.version      = "1.0.9"
  s.summary      = "Gamooga iOS SDK"
  s.description  = "Gamooga iOS SDK"
  s.homepage     = "https://gitlab.com/gamooga-ios/TargetActClientFramework.git"

  s.license      = "Commercial"
  s.author       = "Gamooga"
  s.platform     = :ios, "9.0"
  
  s.source       = { :path => 'https://gitlab.com/gamooga-ios/TargetActClientFramework.git' }
  s.vendored_frameworks = "TargetActClient.framework"
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }


end
